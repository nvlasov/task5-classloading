import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Scanner;

/**
 * @author nv
 */
public class Main {

    public static final Logger LOGGER = Logger.getLogger(Main.class);


    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Integer a = 5;
        Integer b = 2;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose an operation:");
        System.out.println("1. Sum");
        System.out.println("2. Multiplication");
        System.out.print("Enter the number:\t");
        int option = scanner.nextInt();

        if (option == 1) {
            LOGGER.debug("Sum was chosen");
            Sum sum = new Sum();
            int result = sum.sum(5, 2);
            System.out.println("Result:\t" + result);
        } else if (option == 2) {
            LOGGER.debug("Multiplication was chosen");
            Integer result = loadMethodFromJar(a, b);
            System.out.println("Result:\t" + result);
        }
    }

    public static Integer loadMethodFromJar(Integer a, Integer b) throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        LOGGER.debug("New functionality is loading from external JAR ...");
        URL url = new File("jar\\original-classloading-1.0-SNAPSHOT.jar").toURI().toURL();
        URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{url});
        Class multClass = urlClassLoader.loadClass("Multiplication");

        LOGGER.debug("JAR is loaded. Calling method ...");
        Constructor constructor = multClass.getConstructor();
        Object multObject = constructor.newInstance();
        Method method = multClass.getMethod("multiply", Integer.class, Integer.class);
        Object returnObject = method.invoke(multObject, a, b);
        return (Integer) returnObject;
    }

}
