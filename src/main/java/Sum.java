import org.apache.log4j.Logger;

/**
 * @author nv
 */
public class Sum {

    public static final Logger LOGGER = Logger.getLogger(Sum.class);

    public int sum(int a, int b) {
        LOGGER.debug("Sum ...");
        return a + b;
    }

}
